package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts(){
        System.out.println("getProducts");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id ){
        System.out.println("getProductById");
        System.out.println("id es " + id );

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels){
            if (product.getId().equals(id)){
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct){
        System.out.println("createProduct");
        System.out.println("La id nuevo producto es : " + newProduct.getId());
        System.out.println("La descricpcion del nuevo producto es : " + newProduct.getDesc());
        System.out.println("El precio del nuevo producto es : " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);

        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(
            @RequestBody ProductModel product, @PathVariable String id
    ){
        System.out.println("updateProduct");
        System.out.println("La id del producto a actualizar en parametro de URL es : " + id );
        System.out.println("La id del producto a actualizar es : " + product.getId() );
        System.out.println("La descricpcion del producto a actualizar es : " + product.getDesc());
        System.out.println("El precio del producto a actualizar es : " + product.getPrice());

        for (ProductModel productInList : ApitechuApplication.productModels ){
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;


    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("id " + id);
        System.out.println("La id del producto a borrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany == true){
            System.out.println("Borrado producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;

    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(
            @PathVariable String id , @RequestBody ProductModel product
    ){
        System.out.println("patchProduct");
        System.out.println("La id del producto a actualizar en parametro de URL es : " + id );
        System.out.println("La id del producto a actualizar es : " + product.getId() );
        System.out.println("La descricpcion del producto a actualizar es : " + product.getDesc());
        System.out.println("El precio del producto a actualizar es : " + product.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                result = productInList;

                if (product.getDesc().isBlank() == false ){
                    System.out.println("Actualizar la Descripcion del Producto");
                    productInList.setDesc(product.getDesc());
                }
                if (product.getPrice() > 0){
                    System.out.println("Actualizar el precio del Producto");
                    productInList.setPrice(product.getPrice());
                }

            }
        }


        return result ;


    }

}
